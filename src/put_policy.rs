use std::collections::BTreeMap;
use rustc_serialize::json::{self, Json, ToJson};
use signature::SafeBase64;

pub struct PutPolicy {
    scope : Option<String>,
    pub dead_line : Option<u32>,
    pub insert_only : Option<i32>,
    pub save_key : Option<String>,
    pub end_user : Option<String>,
    pub return_url : Option<String>,
    return_body : Option<String>,
    pub callback_url : Option<String>,
    pub callback_host : Option<String>,
    pub callback_body : Option<String>,
    pub callback_bodytype : Option<String>,
    pub callback_fetchkey : Option<i32>,
    pub persistent_ops : Option<String>,
    pub persistent_notifyurl : Option<String>,
    pub persistent_pipeline : Option<String>,
    pub fsize_limit : Option<i64>,
    pub detect_mime : Option<i32>,
    pub mime_limit : Option<String>,
}

impl ToJson for PutPolicy {
    fn to_json(&self)->Json {
        let mut d = BTreeMap::new();

        match self.scope {
            Some(ref s) => d.insert("scope".to_string(), s.to_json()),
            None => None,
        };

        match self.dead_line {
            Some(ref s) => d.insert("deadline".to_string(), s.to_json()),
            None => None,
        };

        match self.insert_only {
            Some(ref s) => d.insert("insertOnly".to_string(), s.to_json()),
            None => None,
        };

        match self.save_key {
            Some(ref s) => d.insert("saveKey".to_string(), s.to_json()),
            None => None,
        };

        match self.end_user {
            Some(ref s) => d.insert("endUser".to_string(), s.to_json()),
            None => None,
        };

        match self.return_url {
            Some(ref s) => d.insert("returnUrl".to_string(), s.to_json()),
            None => None,
        };

        match self.return_body {
            Some(ref s) => d.insert("returnBody".to_string(), s.to_json()),
            None => None, 
        };

        match self.callback_url {
            Some(ref s) => d.insert("callbackUrl".to_string(), s.to_json()),
            None => None,
        };

        match self.callback_host {
            Some(ref s) => d.insert("callbackHost".to_string(), s.to_json()),
            None => None,
        };

        match self.callback_body {
            Some(ref s) => d.insert("callbackBody".to_string(), s.to_json()),
            None => None,
        };

        match self.callback_bodytype {
            Some(ref s) => d.insert("callbackBodyType".to_string(), s.to_json()),
            None => None,
        };

        match self.callback_fetchkey {
            Some(ref s) => d.insert("callbackFetchKey".to_string(), s.to_json()),
            None => None,
        };

        match self.persistent_ops {
            Some(ref s) => d.insert("persistentOps".to_string(), s.to_json()),
            None => None,
        };

        match self.persistent_notifyurl {
            Some(ref s) => d.insert("persistentNotifyUrl".to_string(), s.to_json()),
            None => None,
        };

        match self.persistent_pipeline {
            Some(ref s) => d.insert("persistentPipeline".to_string(), s.to_json()),
            None => None,
        };

        match self.fsize_limit {
            Some(ref s) => d.insert("fsizeLimit".to_string(), s.to_json()),
            None => None,
        };

        match self.detect_mime {
            Some(ref s) => d.insert("detectMime".to_string(), s.to_json()),
            None => None,
        };

        match self.mime_limit {
            Some(ref s) => d.insert("mimeLimit".to_string(), s.to_json()),
            None => None,
        };

        Json::Object(d)
    }
}

impl PutPolicy {
    pub fn create(bucket : &str) -> PutPolicy {
        PutPolicy {
            scope : Some(bucket.to_string()), 
            dead_line : None,
            insert_only : None,
            save_key : None,
            end_user : None,
            return_url : None,
            return_body : None,
            callback_url : None,
            callback_host : None,
            callback_body : None,
            callback_bodytype : None,
            callback_fetchkey : None,
            persistent_ops : None,
            persistent_notifyurl : None,
            persistent_pipeline : None,
            fsize_limit : None,
            detect_mime : None,
            mime_limit : None,
        }
    }



    // pub fn marshal(&self) -> &str {
          
    // }
}

//------------------------------------------------------------------------------
#[cfg(test)]
mod test {

    use put_policy::*;
    use rustc_serialize::json::ToJson;

    #[test]
    fn test_marshal_json() {
      
    }
}











