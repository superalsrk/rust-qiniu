pub struct RSManager;

impl <'a> RSManager {

    pub fn rs_stat() -> &'a str {
        "test"
    }

    pub fn rs_delete() -> &'a str {
        "test"
    }

    pub fn rs_copy() -> &'a str {
        "test"
    }

    pub fn rs_rename() -> &'a str {
        "test"
    }

    pub fn rs_batch_stat() -> &'a str {
        "test"
    }

    pub fn rs_batch_delete() -> &'a str{
        "test"
    }

    pub fn rs_batch_copy() -> &'a str {
        "test"
    }

    pub fn rs_batch_rename() -> &'a str {
        "test"
    }

    pub fn rs_batch_general() -> &'a str {
        "test"
    }
}