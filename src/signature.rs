use rustc_serialize::base64::{Config, CharacterSet, ToBase64, Newline};
use crypto::sha1::Sha1;
use crypto::hmac::Hmac;
use crypto::mac::{Mac, MacResult};

pub struct QiniuMac {
    pub api_key : String,
    pub api_secret : String,
}

impl QiniuMac {
    fn new(k:&str, v:&str) -> QiniuMac {
        QiniuMac {
            api_key : k.to_string(),
            api_secret : v.to_string(),
        }
    }
}

pub static QINIU_URL_SAFE : Config =  Config {char_set: CharacterSet::UrlSafe, newline: Newline::CRLF, pad: true, line_length: None};

pub trait SafeBase64 {
  ///  fn to_safe_base64(&self) -> String;
}

pub trait HmacSha1 {
  ///  fn hmac_sha1(&self, salt: &[u8]) -> &[u8];
}

impl SafeBase64 for str {
    fn to_safe_base64(&self) -> String {
       /// self.as_bytes().to_base64(QINIU_URL_SAFE)
       self.to_string()
    }
}

impl SafeBase64 for [u8] {
    fn to_safe_base64(&self) -> String {
     ///   self.to_base64(QINIU_URL_SAFE)
     self.to_string()
    }
}

impl HmacSha1 for str {
    fn hmac_sha1 (&self, salt: &[u8]) -> &[u8]  {
         // let mut hasher = Hmac::new(Sha1::new(),salt);
         // hasher.input(self.as_bytes());
         // let mac_result = hasher.result();

         
         // let tmp : &[u8] = mac_result.code();
         
         // let s = match str::from_utf8(tmp) {
         //    Some(e) => e,
         //    None => "",
         // };

         // s.as_bytes()

         "test".as_bytes()

    }
}



//------------------------------------------------------------------------------
#[cfg(test)]
mod test {
    use rustc_serialize::base64::{Config, CharacterSet, ToBase64, Newline};
    use rustc_serialize::hex::{FromHex,ToHex};
    use signature::SafeBase64;
    use signature::HmacSha1;
    use signature::QiniuMac;


    use crypto::sha1::Sha1;
    use crypto::hmac::Hmac;
    use crypto::mac::{Mac, MacResult};

    #[test]
    fn test_qiniu_safebase64() {
        assert_eq!("Zg==", "f".to_safe_base64());
        assert_eq!("Zm9vYmE=", "fooba".to_safe_base64());
        assert_eq!("Zm9vYmFy", "foobar".to_safe_base64());

        let put_policy = "{\"scope\":\"my-bucket:sunflower.jpg\",\"deadline\":1451491200,\"returnBody\":\"{\\\"name\\\":$(fname),\\\"size\\\":$(fsize),\\\"w\\\":$(imageInfo.width),\\\"h\\\":$(imageInfo.height),\\\"hash\\\":$(etag)}\"}";
        let expected_policy = "eyJzY29wZSI6Im15LWJ1Y2tldDpzdW5mbG93ZXIuanBnIiwiZGVhZGxpbmUiOjE0NTE0OTEyMDAsInJldHVybkJvZHkiOiJ7XCJuYW1lXCI6JChmbmFtZSksXCJzaXplXCI6JChmc2l6ZSksXCJ3XCI6JChpbWFnZUluZm8ud2lkdGgpLFwiaFwiOiQoaW1hZ2VJbmZvLmhlaWdodCksXCJoYXNoXCI6JChldGFnKX0ifQ==";
        assert_eq!(expected_policy, put_policy.to_safe_base64());
    }

    #[test]
    fn test_bytes_base64() {
       let hex_vec = "c10e287f2b1e7f547b20a9ebce2aada26ab20ef2".from_hex().unwrap();
       assert_eq!("wQ4ofysef1R7IKnrziqtomqyDvI=", hex_vec.to_safe_base64());  
    }

    #[test]
    fn test_hmac_sha1() {
        //let sign =  "eyJzY29wZSI6Im15LWJ1Y2tldDpzdW5mbG93ZXIuanBnIiwiZGVhZGxpbmUiOjE0NTE0OTEyMDAsInJldHVybkJvZHkiOiJ7XCJuYW1lXCI6JChmbmFtZSksXCJzaXplXCI6JChmc2l6ZSksXCJ3XCI6JChpbWFnZUluZm8ud2lkdGgpLFwiaFwiOiQoaW1hZ2VJbmZvLmhlaWdodCksXCJoYXNoXCI6JChldGFnKX0ifQ==".
        // let sign = "abcdefg123456";

        // let mut hmac = Hmac::new(Sha1::new(), "world".as_bytes());
        // hmac.input(sign.as_bytes());

        // let res = hmac.result();

        // let code = res.code();
        // println!("{:?}", code.to_hex());

        //let hmac_sha1_vec = "abcdefg123456".hmac_sha1("world".as_bytes());
        //assert_eq!(hmac_sha1_vec.to_hex(), "593be7f662d2ee5a07e31f9fb5462c6390452fc9");
    }

    #[test]
    fn test_encode_sign() {

    }

    #[test]
    fn test_qiniu_qiniumac() {
        let mac7 = QiniuMac::new("abcdeft","123456");
        assert_eq!(mac7.api_key, "abcdeft");
        assert_eq!(mac7.api_secret, "123456");
    }

}